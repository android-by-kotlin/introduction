package vn.laptrinh.introduction.presentation.component

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource

@Composable
fun ImageComponent(
    modifier: Modifier = Modifier,
    @DrawableRes resId: Int
) {
    Image(
        modifier = modifier,
        painter = painterResource(id = resId),
        contentDescription = null
    )
}