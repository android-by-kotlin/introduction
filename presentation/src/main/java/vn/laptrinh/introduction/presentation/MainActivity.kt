package vn.laptrinh.introduction.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import vn.laptrinh.introduction.presentation.screen.MainScreen
import vn.laptrinh.introduction.presentation.ui.theme.IntroductionTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            IntroductionTheme {
                MainScreen()
            }
        }
    }
}