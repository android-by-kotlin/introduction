package vn.laptrinh.introduction.presentation.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import vn.laptrinh.introduction.presentation.R

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ImagesComponent(
    onImageClick: () -> Unit
) {
    FlowRow(
        modifier = Modifier.verticalScroll(rememberScrollState()),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(32.dp)
    ) {
        ImageComponent(
            modifier = Modifier.size(150.dp),
            resId = R.drawable.ic_android
        )
        ImageComponent(
            modifier = Modifier.size(150.dp),
            resId = R.drawable.ic_kotlin
        )
        ImageComponent(
            modifier = Modifier.size(150.dp),
            resId = R.drawable.ic_youtube
        )
        ImageComponent(
            modifier = Modifier
                .size(150.dp)
                .clickable { onImageClick.invoke() },
            resId = R.drawable.ic_compose
        )
        Icon(
            modifier = Modifier.size(150.dp),
            painter = painterResource(id = R.drawable.ic_android),
            contentDescription = null,
            tint = Color(0xFFFF8C00)
        )
        Icon(
            modifier = Modifier.size(150.dp),
            painter = painterResource(id = R.drawable.ic_kotlin),
            contentDescription = null,
            tint = Color(0xFFFF8C00)
        )
    }
}